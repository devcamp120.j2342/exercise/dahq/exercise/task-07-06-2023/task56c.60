package com.devcamp.customervisitapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.model.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("Nguyen Van A", false, "Basic");
    Customer customer2 = new Customer("Nguyen Van B", false, "Advanced");
    Customer customer3 = new Customer("Nguyen Van C", false, "Basic");

    public ArrayList<Customer> allCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();
        customers.add(customer1);
        customers.add(customer2);
        customers.add(customer3);
        return customers;
    }

}
